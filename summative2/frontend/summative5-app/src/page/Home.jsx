import React from "react";
import Footer from "../container/Footer";
import Header from "../container/Header";
import Content from "../container/Content";

export default class Home extends React.Component {
  render() {
    return (
      <>
        <div id="page">
          <Header />
          <Content />
          <Footer/>
        </div>
      </>
    );
  }
}
