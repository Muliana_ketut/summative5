import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./page/Home";
import "./asset/css/style.css";

function App() {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home}></Route>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
