import React from "react";
import LiFirst from "../component/header/LiFirst";
import LiNav from "../component/header/LiNav";
import Logoimg from "../component/header/Logoimg";

export default class Header extends React.Component {
  state = {
    conFirst: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/headerfirst");
    const body = await response.json();
    this.setState({ conFirst: body });
  }
  render() {
    const { conFirst } = this.state;
    return (
      <>
        <div id="header">
          <Logoimg />
          <ul>
            {conFirst.map((con) => (
              <LiFirst a={con.title} span={con.body} />
            ))}
          </ul>
          <a href="#">Buy tickets / Check Events</a>
          <ul id="navigation">
            <LiNav />
          </ul>
          <img src="images/lion-family.jpg" alt="img" />
          <div>
            <h1>Special Events:</h1>
            <p>
              Masuk Gratis, Keluar Bayar! <a href="#">Event Tidak Terbatas.</a>
            </p>
          </div>
        </div>
      </>
    );
  }
}
