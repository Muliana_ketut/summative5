import React from "react";
import Contactzoo from "../component/footer/Contactzoo";
import LiA from "../component/footer/LiA";
import LiNav from "../component/header/LiNav";

export default class Footer extends React.Component {
  state = {
    conFirst: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/headerfirst");
    const body = await response.json();
    this.setState({ conFirst: body });
  }
  render() {
    const { conFirst } = this.state;
    return (
      <>
        <div id="footer">
          <div>
            <a href="/" className="logo">
              <img src="images/animal-kingdom.jpg" alt="img"></img>
            </a>
            <Contactzoo />
            <ul className="navigation">
              <LiNav />
            </ul>
            <ul>
              {conFirst.map((con) => (
                <LiA a={con.title +" : "+con.body}/>
              ))}
            </ul>
            <p>Copyright &#169; 2021. All Rights Reserved.</p>
          </div>
        </div>
      </>
    );
  }
}
