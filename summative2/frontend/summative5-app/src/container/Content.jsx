import React from "react";
import Content1 from "../component/content/Content1";
import Content2 from "../component/content/Content2";
import Content21 from "../component/content/Content21";
import Content3 from "../component/content/Content3";
import LiAnimals from "../component/content/LiAnimals";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

export default class Footer extends React.Component {
  state = {
    animals: [],
    events: [],
    blogs: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/animals");
    const body = await response.json();
    this.setState({ animals: body });

    const resContent = await fetch("http://localhost:8080/list/events");
    const bodyContent = await resContent.json();
    this.setState({ events: bodyContent });

    const resBlog = await fetch("http://localhost:8080/list/blogs");
    const bodyBlogs = await resBlog.json();
    this.setState({ blogs: bodyBlogs });
  }
  render() {
    const { animals } = this.state;
    const { events } = this.state;
    const { blogs } = this.state;
    return (
      <>
        <div id="content">
          <div id="featured">
            <h2>Meet Our Animals</h2>
            <ul>
              {animals.map((animal) => (
                <LiAnimals src={animal.imgurl} a={animal.name} />
              ))}
            </ul>
          </div>
          <div className="section1">
            <h2>Events</h2>
            <ul id="article">
              {events.map((con) => (
                <Content1 tanggal={con.tanggal} body={con.body} />
              ))}
            </ul>
          </div>
          <div className="section2">
            <h2>Blog : Our Animals</h2>

            {blogs.slice(0, 1).map((blog) => (
              <Content2 name={blog.name} src={blog.imgurl} body={blog.body} />
            ))}
            <div id="section1">
              <ul>
                {blogs.slice(1, 3).map((blog) => (
                  <Content21
                    name={blog.name}
                    src={blog.imgurl}
                    body={blog.body}
                  />
                ))}
              </ul>
            </div>
            <div id="section2">
              <ul>
                {blogs.slice(3, 4).map((blog) => (
                  <Content21
                    name={blog.name}
                    src={blog.imgurl}
                    body={blog.body}
                  />
                ))}
              </ul>
            </div>
          </div>
          <div className="section3">
            <Content3 />
          </div>
        </div>
      </>
    );
  }
}
