import React from "react";

export default class LiFirst extends React.Component {
  render() {
    return (
      <>
        <li className="first">
          <h2>
            <a>{this.props.a}</a>
          </h2>
          <span>{this.props.span}</span>
        </li>
      </>
    );
  }
}
