import React from "react";
import { NavLink } from "react-router-dom";

export default class LiNav extends React.Component {
  render() {
    return (
      <>
        <li id="link1" className="selected">
          <NavLink exact to="/">
            Home
          </NavLink>
        </li>
        <li id="link2">
          <NavLink exact to="/">
            The zoo
          </NavLink>
        </li>
        <li id="link3">
          <NavLink exact to="/">
            Visitors Info
          </NavLink>
        </li>
        <li id="link4">
          <NavLink exact to="/">
            Tickets
          </NavLink>
        </li>
        <li id="link5">
          <NavLink exact to="/">
            Events
          </NavLink>
        </li>
        <li id="link6">
          <NavLink exact to="/">
            Galerry
          </NavLink>
        </li>
        <li id="link7">
          <NavLink exact to="/">
            Contact Us
          </NavLink>
        </li>
      </>
    );
  }
}
