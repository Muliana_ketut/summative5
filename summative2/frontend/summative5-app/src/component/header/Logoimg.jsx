import React from "react";

export default class Logoimg extends React.Component {
  render() {
    return (
      <>
        <a id="logo">
          <img src="/images/logo.jpg" alt="img"></img>
        </a>
      </>
    );
  }
}
