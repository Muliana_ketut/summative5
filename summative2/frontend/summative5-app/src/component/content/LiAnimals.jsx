import React from "react";

export default class LiAnimals extends React.Component {
  render() {
    return (
      <>
        <li className="first">
          <a href="#">
            <img src={this.props.src} alt="img"></img>
          </a>
          <a href="#">{this.props.a}</a>
        </li>
      </>
    );
  }
}
