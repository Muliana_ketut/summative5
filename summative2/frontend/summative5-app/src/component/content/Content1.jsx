import React from "react";

export default class Content1 extends React.Component {
  render() {
    return (
      <>
        <li className="first">
          <a href="#">
            <span>{this.props.tanggal}</span>
          </a>
          <p>{this.props.body}</p>
        </li>
      </>
    );
  }
}
