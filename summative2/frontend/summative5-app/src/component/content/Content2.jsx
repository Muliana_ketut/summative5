import React from "react";

export default class Content2 extends React.Component {
  render() {
    return (
      <>
        <p>{this.props.name}</p>
        <a href="#">
          <img src={this.props.src}></img>
        </a>
        <ul>
          <li>
            <p>{this.props.body}</p>
          </li>
        </ul>
      </>
    );
  }
}
