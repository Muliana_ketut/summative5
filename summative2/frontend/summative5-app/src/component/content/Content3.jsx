import React from "react";
import FormS3 from "../../atomicComponent/FormS3";
import Medsos from "../../atomicComponent/Medsos";

export default class Content3 extends React.Component {
  render() {
    return (
      <>
        <h2>Connect</h2>
        <Medsos/>
        <FormS3/>
        <img src="images/penguin2.jpg" alt="img"></img>
      </>
    );
  }
}
