import React from "react";

export default class Content21 extends React.Component {
  render() {
    return (
      <>
        <li>
          <a>
            <img src={this.props.src} alt="img"></img>
          </a>
          <h4>
            <a>{this.props.name}</a>
          </h4>
          <p>{this.props.body}</p>
        </li>
      </>
    );
  }
}
