import React from "react";

export default class LiA extends React.Component {
  render() {
    return (
      <>
        <li>
          <a>{this.props.a}</a>
        </li>
      </>
    );
  }
}
