package com.summative5.codesum5.controller;

import java.util.List;

import com.summative5.codesum5.modal.Animal;
import com.summative5.codesum5.modal.Descanimals;
import com.summative5.codesum5.modal.Eventszoo;
import com.summative5.codesum5.modal.Headerfirst;
import com.summative5.codesum5.repository.AnimalRepository;
import com.summative5.codesum5.repository.DescanimalsRepository;
import com.summative5.codesum5.repository.EventszooRepository;
import com.summative5.codesum5.repository.HeaderfirtsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/list/")
public class GetControllerApp {
    @Autowired
    private HeaderfirtsRepository headerfirtsRepository;

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private EventszooRepository eventszooRepository;

    @Autowired
    private DescanimalsRepository descanimalsRepository;

    @GetMapping("/headerfirst")
    public List<Headerfirst> listBlogmaincon() {
        return headerfirtsRepository.findAll();
    }

    @GetMapping("/animals")
    public List<Animal> listAnimals() {
        return animalRepository.findAll();
    }

    @GetMapping("/events")
    public List<Eventszoo> listEventszoo() {
        return eventszooRepository.findAll();
    }

    @GetMapping("/blogs")
    public List<Descanimals> listBlogs() {
        return descanimalsRepository.findAll();
    }
}
