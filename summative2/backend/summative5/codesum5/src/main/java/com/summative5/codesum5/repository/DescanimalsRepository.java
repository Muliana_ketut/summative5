package com.summative5.codesum5.repository;

import com.summative5.codesum5.modal.Descanimals;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DescanimalsRepository extends JpaRepository<Descanimals, Integer> {
    
}
