package com.summative5.codesum5.repository;

import com.summative5.codesum5.modal.Headerfirst;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HeaderfirtsRepository extends JpaRepository<Headerfirst, Integer> {
    
}
