package com.summative5.codesum5.repository;

import com.summative5.codesum5.modal.Eventszoo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EventszooRepository extends JpaRepository<Eventszoo, Integer> {
    
}
