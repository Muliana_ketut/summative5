package com.summative5.codesum5.repository;

import com.summative5.codesum5.modal.Animal;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AnimalRepository extends JpaRepository<Animal, Integer> {
    
}
