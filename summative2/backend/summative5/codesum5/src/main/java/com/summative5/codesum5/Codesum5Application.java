package com.summative5.codesum5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Codesum5Application {

	public static void main(String[] args) {
		SpringApplication.run(Codesum5Application.class, args);
	}

}
